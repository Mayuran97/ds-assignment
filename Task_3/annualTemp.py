import csv
import pymongo

mongoClient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = mongoClient["GlobalTemperature"]
mycol = mydb["AnnualTemperature"]

with open('AnnualGlobalTemperature.csv')as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')

    thislist = []

    for row in readCSV:

        thisdict = {

            "upper_95_ci": row[1],
            "lower_95_ci": row[2],
        }
        thislist.append(thisdict)
        mycol.insert_one(thisdict)






