const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;

const connectionURL = 'mongodb://localhost:27017';
const databaseName = 'shop'

MongoClient.connect(connectionURL,
  {useUnifiedTopology: true}, 
  function(error, client) {
  
      if (error!= null){
      console.log(error);
      }else{

      const db = client.db(databaseName);
  
  const query = { address: /^H/ };
  db.collection("customers").find(query).toArray(function(error, result) {
      if (error) throw error;
      console.log(result);
      client.close();
      }
    );
    }
  }
);